using Microsoft.AspNetCore.Mvc;

namespace MyDLPAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class NCStatusController : ControllerBase
{
    private readonly ILogger<NCStatusController> _logger;
    private readonly IHttpContextAccessor _accessor;

    public NCStatusController(IHttpContextAccessor accessor, ILogger<NCStatusController> logger)
    {
        _accessor = accessor;
        _logger = logger;
    }

    [HttpPost(Name = "PostNCStatus")]
    public IActionResult Post(NCStatus status)
    {
        if (!ModelState.IsValid)
            return BadRequest(ModelState);
        var ip = "unknown";
        var filename = "";

        ip = _accessor?.HttpContext?.Connection?.RemoteIpAddress?.ToString();
        if (ip != null)
            ip = ip.Replace(":", "").Replace(".", "-");
        filename = Path.Combine(Environment.CurrentDirectory, $"{ip}_{DateTimeOffset.Now.ToUnixTimeMilliseconds()}.log");
        System.Console.WriteLine($"[{DateTimeOffset.Now}] Request from: {ip}\n\t\t\tCreting Log: '{filename}'");

        ThreadPool.QueueUserWorkItem(w =>
        {
            try { System.IO.File.WriteAllText(filename, $"{status}"); }
            catch (Exception e) { System.Console.WriteLine(e); }
        });
        return Ok();
    }

    public class NCStatus
    {
        public string? raw_data { get; set; } // base64 string please!!
        public string? org { get; set; }
        public override string ToString()
        {
            return $"{System.Text.Json.JsonSerializer.Serialize(this)}";
        }
    }
}