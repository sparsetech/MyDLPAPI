var appPath = System.Diagnostics.Process.GetCurrentProcess().MainModule?.FileName;
if (appPath != null)
{
    var appInfo = new FileInfo(appPath);
    // Set current path
    if (appInfo.DirectoryName != null)
        Directory.SetCurrentDirectory(appInfo.DirectoryName);
}

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddHttpContextAccessor();
//builder.Services.AddSingleton<MyDLPAPI.Controllers.NCStatusController>();
builder.Services.AddControllers();

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();
