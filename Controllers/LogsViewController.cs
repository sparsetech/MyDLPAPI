﻿using Microsoft.AspNetCore.Mvc;

namespace MyDLPAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class LogsViewController : ControllerBase
{
    private readonly ILogger<NCStatusController> _logger;
    private readonly IHttpContextAccessor _accessor;

    public LogsViewController(IHttpContextAccessor accessor, ILogger<NCStatusController> logger)
    {
        _accessor = accessor;
        _logger = logger;
    }

    [HttpGet]
    public IActionResult Get()
    {
        List<string> ls = new List<string>();
        try
        {
            foreach (var item in Directory.GetFiles(Environment.CurrentDirectory))
            {
                var f = new FileInfo(item);
                if (f.Extension == ".log")
                    ls.Add(f.Name);
            }
        }
        catch (Exception ex) { _logger.LogError($"{ex}"); }
        return new JsonResult(ls);
    }

    [HttpGet("GetByFile/{file}")]
    public IActionResult GetByFile(string file)
    {
        try
        {
            var f = Path.Combine(Environment.CurrentDirectory, file);
            if (System.IO.File.Exists(f))
                return new JsonResult(System.IO.File.ReadAllText(f));
        }
        catch (Exception ex) { _logger.LogError($"{ex}"); }
        return NotFound();
    }

    [HttpGet("GetRawDataByFile/{file}")]
    public IActionResult GetRawDataByFile(string file)
    {
        try
        {
            var f = Path.Combine(Environment.CurrentDirectory, file);
            if (System.IO.File.Exists(f))
            {
                var d = System.IO.File.ReadAllText(f);
                var ncs = System.Text.Json.JsonSerializer.Deserialize<NCStatusController.NCStatus>(d);
                if (ncs != null && ncs.raw_data != null)
                {
                    var json = Base64Decode(ncs.raw_data);
                    return new JsonResult(json);
                }
            }
        }
        catch (Exception ex) { _logger.LogError($"{ex}"); }
        return NotFound();
    }

    public static string Base64Decode(string data)
    {
        var baseBytes = System.Convert.FromBase64String(data);
        return System.Text.Encoding.UTF8.GetString(baseBytes);
    }
}